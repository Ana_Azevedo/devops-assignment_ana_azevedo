node {
    // define a random name for the test image
    def TEST_IMAGE_NAME = UUID.randomUUID().toString()

    // define a name for the release image
    def RELEASE_IMAGE_NAME = "margaridaduazevedo/devops-pipeline-assignment_p2"

    def testImage

    stage('Clone repository') {
        /* Let's make sure we have the repository cloned to our workspace */
        checkout scm
    }

     // the next steps are inside a try/catch, so that we have a change to cleanup
    try {

        stage('Build') {
            /* This builds the actual image using the Dockerfile in the repo. 
               One could do this instead:
                 sh "docker build -t $TEST_IMAGE_NAME . "
            */
            testImage = docker.build("margaridaduazevedo/devops-pipeline-assignment_p1")
        }

        stage('UnitTest') {
            /* This runs the tests. 
               One could do this instead:
                 sh "docker run -w /simple-calculator $TEST_IMAGE_NAME mvn test"
            */    
            testImage.inside("-w /devops-pipeline-assignment") {
                sh 'mvn test'
            }    
        }
		
		 stage('IntegrationTest') {
            /* This runs the tests. 
               One could do this instead:
                 sh "docker run -w /simple-calculator $TEST_IMAGE_NAME mvn test"
            */    
            testImage.inside("-w /devops-pipeline-assignment") {
                sh 'mvn verify -P integration-test'
            }    
        }

        stage('Release') {
            /* This packages the jar and build a "release" docker image that runs the jar. */  

            // 1. Create the jar;
            testImage.inside("-w /devops-pipeline-assignment") {
                
            sh """
              mvn package
              cp target/maven-integration-testing-jar-with-dependencies.jar parte2.jar
            """    
            }

            // 2. Create "release" docker image with that executes the jar when run (no source code in this image)
            def releaseImage = docker.build("$RELEASE_IMAGE_NAME", "-f Dockerfile.release .")

            // docker hub credenticals are configured in Jnekins with name "isepswitch-docker-hub"
            withCredentials([usernamePassword(credentialsId: 'anadockerhub', passwordVariable: 'DOCKERHUBPASS', usernameVariable: 'DOCKERHUBUSERNAME')]) {
				sh """
                    echo $DOCKERHUBPASS | docker login -u $DOCKERHUBUSERNAME --password-stdin
                    docker push $RELEASE_IMAGE_NAME
                """  
			}

        }

        stage('Cleanup') {
            /* Remove images */
            sh "docker rmi $TEST_IMAGE_NAME $RELEASE_IMAGE_NAME"
        }

     } catch (exc) {
        sh "docker rmi $TEST_IMAGE_NAME $RELEASE_IMAGE_NAME"
     }    
}